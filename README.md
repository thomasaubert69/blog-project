#                            Projet Blog

Presentation du projet : Le but de ce projet était de crée un blog.

La liste des outils utiliser comportait : 

* PHP
* Framework Symfony avec Twig pour l'html.
* PDO/SQL

Les fonctionnalités attendu lors de ce projet étaient de pouvoir créer des articles, consulter la liste des articles, consulter un article specifique, et modifier ou supprimer un article.

##                                      Maquettes

![](./public/maquettepc.png)

![](./public/maquettepc2.png)

![](./public/maquettetel.png)

