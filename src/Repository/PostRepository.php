<?php

namespace App\Repository;

use App\Entity\Post;
use DateTime;

/**
 * Un Repository, ou DAO (terme plus "pro", Data Access Object) est une
 * class dont le but est de concentré les appels à la base de données,
 * ainsi, le reste de l'application dépendra des DAO et on aura pas 
 * des appels bdd disseminés partout dans l'appli (comme ça, si jamais 
 * la table change, on le sgbd ou autre, on aura juste le DAO à 
 * modifier et pas tous les endroits où on aurait fait ces appels)
 */

class PostRepository
{

    private $pdo;

    public function __construct()
    {

        $this->pdo = new \PDO(
            'mysql:host=localhost;dbname=blog',
            'simplon',
            '1234'
        );
    }


    /**
     * Méthode qui va aller chercher toutes les personnes
     * présentent dans la base de données et les convertir
     * en instance de la classe Post
     * @return Post[] les posts contenues dans la bdd
     */
    public function findAll(): array
    {

        $query = $this->pdo->prepare('SELECT * FROM post');

        $query->execute();

        $results = $query->fetchAll();
        $list = [];

        foreach ($results as $line) {

            $post = $this->sqlToPost($line);

            $list[] = $post;
        }

        return $list;
    }


    public function add(string $title, string $author, string $content): Int
    {

        $query = $this->pdo->prepare('INSERT INTO post (title,author,content) VALUES (:title,:author,:content)');

        $query->bindValue('title', $title, \PDO::PARAM_STR);
        $query->bindValue('author', $author, \PDO::PARAM_STR);
        // $query->bindValue('postDate', $date->getPostDate()->format('Y/m/d H:i:s'), \PDO::PARAM_STR);
        $query->bindValue('content', $content, \PDO::PARAM_STR);

        $query->execute();

        return $this->pdo->lastInsertId();
    }


    public function findById(int $id): Post
    {

        $query = $this->pdo->prepare('SELECT * FROM post WHERE id = :id');

        $query->bindValue(':id', $id, \PDO::PARAM_INT);

        $query->execute();

        $line = $query->fetch();

        if ($line) {

            return $this->sqlToPost($line);
        }

        return null;
    }

    private function sqlToPost(array $line): Post
    {
        return new Post($line['title'], $line['author'], $line["content"], new \DateTime($line['postDate']), $line["id"]);
    }

    public function delete(int $id): void
    {
        $query = $this->pdo->prepare('DELETE FROM post WHERE id = :id');
        $query->bindValue(':id', $id, \PDO::PARAM_INT);
        $query->execute();
    }
}
