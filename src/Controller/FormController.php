<?php

namespace App\Controller;

use DateTime;
use App\Entity\Post;
use App\Repository\PostRepository;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class FormController extends AbstractController
{

    /** cherche les post dans la database
     * @Route("/", name="Home")
     */
    public function searchPost(PostRepository $repo)
    {
        $newPost = $repo->findAll();

        return $this->render('Blog.html.twig', [
            'post' => $newPost
        ]);
    }


/**
 * @Route("/post/{id}", name ="showPost")
 */
public function showPost(int $id) {
    $repo = new PostRepository();
    $post = $repo->findById($id);
    return $this->render('showPost.html.twig', [
        'post' => $post
    ]);
}





    /** 
     * @Route("/addPost", name="addPost")
     */
    public function addPost(Request $request)
    {
        

        $repo = new PostRepository();
        
        

        if ($request->get('title') && $request->get('content')) {
            
            $id = $repo->add($request->get('title'), $request->get('author'), $request->get('content'));
            return $this->redirectToRoute('Home', ['id' => $id]);
        }
        else {
            return $this->render('addPost.html.twig');
        }


    }



    /**
     * @Route("/del/{id}", name="deletepost")
     */
    public function deletePost(int $id)
    {
        $repo = new PostRepository();
        $repo->delete($id);
        return $this->redirectToRoute('Home');
    }
}
